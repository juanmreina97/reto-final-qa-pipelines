package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.models.VacancyModel;
import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static com.orangehrmlive.question.fill.VacancyAdded.vacancyAdded;
import static com.orangehrmlive.question.fill.VacancyNotAdded.vacancyNotAddedWithVisibleMessageInvalidHiringManagerField;
import static com.orangehrmlive.task.browse.BrowseBackToVacancySection.browseBackToVacancySection;
import static com.orangehrmlive.task.browse.BrowseToFillVacancy.browseToFillVacancy;
import static com.orangehrmlive.task.fill.FillLogIn.fillLogIn;
import static com.orangehrmlive.task.fill.FillVacancyPage.fillVacancyPage;
import static com.orangehrmlive.util.Dictionary.*;
import static com.orangehrmlive.util.GeneralData.vacancyModelData;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class ManageVacanciesTestStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(ManageVacanciesTestStepDefinition.class);
    private VacancyModel vacancyModel;

    @When("I login and enter to the vacancies section")
    public void iLoginAndEnterToTheVacanciesSection() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(PASS),

                    browseToFillVacancy()
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @When("I add a new vacancy job information and save it")
    public void iAddANewVacancyJobInformationAndSaveIt() {
        vacancyModel = vacancyModelData();
        try{
            theActorInTheSpotlight().attemptsTo(
                    fillVacancyPage()
                            .selectingTheJobTitle(vacancyModel.getJobTitle())
                            .typingTheVacancyName(vacancyModel.getVacancyName())
                            .typingTheHiringManager(vacancyModel.getHiringManager())
                            .typingTheNumberOfPositions(vacancyModel.getNumberOfPositions())
                            .typingTheJobDescription(vacancyModel.getDescription()),
                    browseBackToVacancySection()
                            .andSelectingTheVacancyName(vacancyModel.getVacancyName())
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @Then("The job Vacancy should appear in the Vacancies section")
    public void theJobVacancyShouldAppearInTheVacanciesSection() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            vacancyAdded()
                                    .wasValidatedWithVacancyName(vacancyModel.getVacancyName())
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            LOGGER.warn(ADD_VACANCY_VALIDATION_ERROR);
        }
        LOGGER.info(ADD_VACANCY_DONE);
    }

    @When("I add new vacancy job information without Hiring Manager Name and try to save it")
    public void iAddNewVacancyJobInformationWithoutHiringManagerNameAndTryToSaveIt() {
        vacancyModel = vacancyModelData();
        try{
            theActorInTheSpotlight().attemptsTo(
                    fillVacancyPage()
                            .selectingTheJobTitle(vacancyModel.getJobTitle())
                            .typingTheVacancyName(vacancyModel.getVacancyName())
                            .typingTheHiringManager(EMPTY_STRING)
                            .typingTheNumberOfPositions(vacancyModel.getNumberOfPositions())
                            .typingTheJobDescription(vacancyModel.getDescription())
            );
        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @Then("I should not be able to save vacancy")
    public void iShouldNotBeAbleToSaveVacancy() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            vacancyNotAddedWithVisibleMessageInvalidHiringManagerField()
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            LOGGER.warn(NOT_ADDED_VACANCY_VALIDATION_ERROR);
        }
        LOGGER.info(NOT_ADDED_VACANCY_DONE);
    }
}
