package webservices.org.oorsprong.stepdefinition.countryflag;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import webservices.org.oorsprong.stepdefinition.SetupLog4j2;


import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.containsString;
import static webservices.org.oorsprong.question.countrybyname.SoapResponse.response;
import static webservices.org.oorsprong.task.countrybyname.DoPost.doPost;
import static webservices.org.oorsprong.util.CountryISOCodeKey.COUNTRY_ISO_CODE;
import static webservices.org.oorsprong.util.Dictionary.*;
import static webservices.org.oorsprong.util.Utilities.readFile;


public class CountryFlagStepDefinition extends SetupLog4j2 {

    private final Map<String, Object> headers = new HashMap<>();
    private final Actor actor = Actor.named("juan");
    private String bodyRequest;
    private static final Logger LOGGER = Logger.getLogger(CountryFlagStepDefinition.class);

    @Given("that the user is using the web resource and using the country code {string}")
    public void thatTheUserIsUsingTheWebResourceAndUsingTheCountryCode(String countryIsoCode){
        try {
            setup();
            actor.whoCan(CallAnApi.at(URL_BASE));
            headers.put("Content-Type", "text/xml;charset=UTF-8");
            headers.put("SOAPAction", "");
            bodyRequest = defineBodyRequest(countryIsoCode);
            LOGGER.info("Se hara un request al sitio " + URL_BASE + " con los siguientes datos:" + headers.toString() + bodyRequest);
        }
        catch(Exception e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @When("the user generates the required query")
    public void theUserGeneratesTheRequiredQuery() {
        try{
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE)
                            .withHeaders(headers)
                            .andBodyRequest(bodyRequest)
            );
            LOGGER.info("Se hace el request");
        }
        catch(Exception e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Then("he will see the file name with the flag")
    public void heWillSeeTheFileNameWithTheFlag() {
        try {
            LOGGER.info("la respuesta fue " + LastResponse.received().answeredBy(actor).asString());
            actor.should(
                    seeThatResponse("El codigo de respuesta debe ser " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                    ),
                    seeThat("El nombre del archivo debe ser: " + FLAG_FILE_AN_RETURN,
                            response(), containsString(FLAG_FILE_AN_RETURN)
                    )
            );
        }catch(Exception e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Then("the resource send the error message: {string}")
    public void theResourceSendTheErrorMessage(String errorMessage) {
        try {
            LOGGER.info("La respuesta erronea fue" + LastResponse.received().answeredBy(actor).asString());
            actor.should(
                    seeThatResponse("El codigo de respuesta debe ser " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                    ),
                    seeThat("El mensaje jenkins 25  del servidor debe ser " + errorMessage,
                            response(), containsString(errorMessage)
                    )
            );
        }
        catch(Exception e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }

    protected String defineBodyRequest(String countryIsoCode)
    {
        return readFile(COUNTRY_FLAG_ISO_CODE_XML_LINUX)
                .replace(COUNTRY_ISO_CODE.getValue(), countryIsoCode);
    }
}
