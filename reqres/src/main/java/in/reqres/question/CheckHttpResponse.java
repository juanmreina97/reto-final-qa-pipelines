package in.reqres.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.log4j.Logger;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class CheckHttpResponse implements Question<Void> {
    private int expectedStatusCode;
    private static final Logger LOGGER = Logger.getLogger(CheckHttpResponse.class);

    public static CheckHttpResponse checkHttpResponse() {
        return new CheckHttpResponse();
    }

    public CheckHttpResponse setExpectedResponse(int expectedStatusCode) {
        this.expectedStatusCode = expectedStatusCode;
        return this;
    }

    @Override
    public Void answeredBy(Actor actor) {
        LOGGER.info("Response body:\n");
        LastResponse.received().answeredBy(actor).prettyPrint();
        actor.should(
                seeThatResponse("Code response would be: " + expectedStatusCode,
                        validateResponse -> validateResponse.statusCode(expectedStatusCode)
                )
        );
        return null;
    }
}
