package in.reqres.stepdefinitions.get;

import in.reqres.models.resource.Resource;
import in.reqres.stepdefinitions.setup.BaseResources;
import in.reqres.util.GetResources;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.StringResponse.stringResponse;
import static in.reqres.question.get.SingleResourceResponse.singleResourceResponse;
import static in.reqres.task.get.DoGetWithParam.doGetWithParam;
import static in.reqres.util.RandomHelper.randomNonExistUserId;
import static in.reqres.util.RandomHelper.randomUserId;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;


public class GetSingleResourceStepDefinition extends BaseResources {

    private static final Logger LOGGER = Logger.getLogger(GetSingleResourceStepDefinition.class);
    private final Actor actor = Actor.named("user");

    private Resource resultingResource = new Resource();

    @Given("the administrator enter on the page")
    public void the_administrator_enter_on_the_page() {
        generalSetUp();
        try {
            actor.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }
    }


    @When("enter the id of the resource")
    public void enter_the_id_of_the_resource() {
        String userId = randomUserId();
        try {

            actor.attemptsTo(
                    doGetWithParam()
                            .usingTheResource(GetResources.VIEW_SINGLE_RESOURCE.getValue())
                            .setParamAndValue("id", userId)
            );
            LOGGER.info("Successful access to GET resource: ".concat(GetResources.VIEW_SINGLE_RESOURCE.replaceParam(userId)));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting GET resource: ".concat(GetResources.VIEW_SINGLE_RESOURCE.replaceParam(userId)));
            Assertions.fail(exception);
        }

    }


    @Then("the characteristics of the resource should be displayed")
    public void the_characteristics_of_the_resource_should_be_displayed() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_OK).answeredBy(actor);
        resultingResource = singleResourceResponse().answeredBy(actor);
        actor.should(
                seeThat("the received data is: ",
                        response -> resultingResource,
                        hasProperty("dataResource",
                                allOf(
                                        hasProperty("id"),
                                        hasProperty("name"),
                                        hasProperty("year"),
                                        hasProperty("color"),
                                        hasProperty("pantoneValue")
                                )
                        )
                )
        );
    }

    @When("you enter the id of the nonexistent resource")
    public void you_enter_the_id_of_the_nonexistent_resource() {
        String userId = randomNonExistUserId();
        try {

            actor.attemptsTo(
                    doGetWithParam()
                            .usingTheResource(GetResources.VIEW_SINGLE_RESOURCE.getValue())
                            .setParamAndValue("id", userId)
            );
            LOGGER.info("Successful access to GET resource: ".concat(GetResources.VIEW_SINGLE_RESOURCE.replaceParam(userId)));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting GET resource: ".concat(GetResources.VIEW_SINGLE_RESOURCE.replaceParam(userId)));
            Assertions.fail(exception);
        }

    }

    @Then("the characteristics of the resource should not be displayed")
    public void the_characteristics_of_the_resource_should_not_be_displayed() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_NOT_FOUND).answeredBy(actor);

        actor.should(
                seeThat("no reply message",
                        stringResponse(), isEmptyString())
        );

    }

}
