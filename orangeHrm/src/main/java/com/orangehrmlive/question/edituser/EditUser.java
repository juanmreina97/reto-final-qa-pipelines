package com.orangehrmlive.question.edituser;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.myinfointerface.MyInfoInterface.FIRST_NAME_DETAILS;

public class EditUser implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return FIRST_NAME_DETAILS.resolveFor(actor).isVisible();
    }
    public EditUser is(){
        return this;
    }

    public static EditUser editUserValidatedWithNewNameAdd(){
        return new EditUser();
    }

}
