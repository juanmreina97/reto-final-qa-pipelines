package in.reqres.util;


public enum PutResources implements ReplaceStringRegex {
    UPDATE_USER_WITH_PUT("/api/users/{id}");

    private final String value;

    PutResources(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String replaceParam(String param) {
        return replaceString(value, param);
    }
}
