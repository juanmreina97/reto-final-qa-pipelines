package in.reqres.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({
        "url",
        "text"
})
public class Support {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("url")
    private String url;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("text")
    private String text;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}