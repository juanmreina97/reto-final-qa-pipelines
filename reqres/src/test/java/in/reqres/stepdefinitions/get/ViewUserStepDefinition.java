package in.reqres.stepdefinitions.get;

import in.reqres.models.user.User;
import in.reqres.stepdefinitions.setup.BaseResources;
import in.reqres.util.GetResources;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.get.SingleUserResponse.singleUserResponse;
import static in.reqres.question.StringResponse.stringResponse;
import static in.reqres.task.get.DoGetWithParam.doGetWithParam;
import static in.reqres.util.RandomHelper.randomNonExistUserId;
import static in.reqres.util.RandomHelper.randomUserId;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;

public class ViewUserStepDefinition extends BaseResources {

    private static final Logger LOGGER = Logger.getLogger(ViewUserStepDefinition.class);
    private User resultingUser = new User();
    private final Actor actor = Actor.named("User");

    @Given("the administrator enters the page")
    public void theAdministratorEntersThePage() {
        generalSetUp();
        try {
            actor.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }
    }

    @When("the administrator enters the user id")
    public void theAdministratorEntersTheUserId() {

        String userId = randomUserId();
        try {
            actor.attemptsTo(
                    doGetWithParam()
                            .usingTheResource(GetResources.VIEW_SINGLE_USER.getValue())
                            .setParamAndValue("id", userId)
            );
            LOGGER.info("Successful access to GET resource: ".concat(GetResources.VIEW_SINGLE_USER.replaceParam(userId)));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting GET resource: ".concat(GetResources.VIEW_SINGLE_USER.replaceParam(userId)));
            Assertions.fail(exception);
        }
    }

    @Then("the administrator views the user information")
    public void theAdministratorViewsTheUserInformation() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_OK).answeredBy(actor);
        resultingUser = singleUserResponse().answeredBy(actor);

        actor.should(
                seeThat("he has a value in each field ",
                        response -> resultingUser,
                        hasProperty("dataUser",
                                allOf(
                                        hasProperty("id"),
                                        hasProperty("email"),
                                        hasProperty("firstName"),
                                        hasProperty("lastName"),
                                        hasProperty("avatar")
                                ))
                )
        );
    }

    @When("the administrator enters the id of the nonexistent user")
    public void theAdministratorEntersTheIdOfTheNonexistentUser() {
        String userId = randomNonExistUserId();
        try {
            actor.attemptsTo(
                    doGetWithParam()
                            .usingTheResource(GetResources.VIEW_SINGLE_USER.getValue())
                            .setParamAndValue("id", userId)
            );
            LOGGER.info("Successful access to GET resource: ".concat(GetResources.VIEW_SINGLE_USER.replaceParam(userId)));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting GET resource: ".concat(GetResources.VIEW_SINGLE_USER.replaceParam(userId)));
            Assertions.fail(exception);
        }
    }

    @Then("the administrator does not see any information")
    public void theAdministratorDoesNotSeeAnyInformation() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_NOT_FOUND).answeredBy(actor);
        actor.should(
                seeThat("the response message", stringResponse(), isEmptyString())
        );
    }
}