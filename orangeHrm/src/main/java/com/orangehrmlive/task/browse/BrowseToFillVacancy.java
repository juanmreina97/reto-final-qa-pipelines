package com.orangehrmlive.task.browse;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouseToTarget;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.vacancypage.VacancyPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class BrowseToFillVacancy implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the           (RECRUITMENT_MENU,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to               (RECRUITMENT_MENU),
                new MoveMouseToTarget(RECRUITMENT_MENU),

                Scroll.to               (VACANCIES),
                Click.on                (VACANCIES),

                WaitUntil.the   (ADD_BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),
                Scroll.to               (ADD_BTN),
                Click.on                (ADD_BTN)
        );
    }

    public static BrowseToFillVacancy browseToFillVacancy(){
        return new BrowseToFillVacancy();
    }
}
