package in.reqres.util;

public enum PostResources implements ReplaceStringRegex {
    CREATE_USER_WITH_POST("/api/users"),
    REGISTER_USER("/api/register");

    private final String value;

    PostResources(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String replaceParam(String param) {
        return replaceString(value, param);
    }

}
