package com.orangehrmlive.userinterface.qualificationpage;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class QualificationPage extends PageObject {

    public static final Target MY_INFO_MENU = Target
            .the("My Info menu")
            .located(By.id("menu_pim_viewMyDetails"));

    public static final Target QUALIFICATIONS = Target
            .the("Qualifications")
            .located(By.xpath("//*[@id=\"sidenav\"]/li[10]/a"));

    public static final Target ADD_WORK_EXPERIENCE_BTN = Target
            .the("Add Work Experience Button")
            .located(By.id("addWorkExperience"));

    public static final Target DELETE_WORK_EXPERIENCE_BTN = Target
            .the("Delete Work Experience Button")
            .located(By.id("delWorkExperience"));

    public static final Target SELECT_ALL_WORKS = Target
            .the("Select All work experience")
            .located(By.xpath("//*[@id=\"workCheckAll\"]"));

    public static final Target EXPERIENCE_COMPANY = Target
            .the("Experience Company Name")
            .located(By.id("experience_employer"));

    public static final Target EXPERIENCE_JOB_TITLE = Target
            .the("Experience Job Title")
            .located(By.id("experience_jobtitle"));

    public static final Target EXPERIENCE_FROM_DATE = Target
            .the("Experience From Date")
            .located(By.id("experience_from_date"));

    public static final Target MONTH_DATE = Target
            .the("Month Date")
            .located(By.xpath("//*[@class=\"ui-datepicker-month\"]"));

    public static final Target YEAR_DATE = Target
            .the("Year Date")
            .located(By.xpath("//*[@class=\"ui-datepicker-year\"]"));

    public static final Target DAY_DATE = Target
            .the("Day Date")
            .located(By.xpath("//table[@class=\"ui-datepicker-calendar\"]//tr[3]//td[3]"));

    public static final Target EXPERIENCE_TO_DATE = Target
            .the("Experience To Date")
            .located(By.id("experience_to_date"));

    public static final Target EXPERIENCE_COMMENT = Target
            .the("Experience Comment")
            .located(By.id("experience_comments"));

    public static final Target WORK_EXPERIENCE_SAVE_BTN = Target
            .the("Work Experience Save Button")
            .located(By.id("btnWorkExpSave"));

    public static final Target ADD_EDUCATION_BTN = Target
            .the("Add Education Experience Button")
            .located(By.id("addEducation"));

    public static final Target DELETE_EDUCATION_BTN = Target
            .the("Delete Education Experience Button")
            .located(By.id("delEducation"));

    public static final Target SELECT_ALL_EDUCATIONS = Target
            .the("Select All Education experience")
            .located(By.xpath("//*[@id=\"educationCheckAll\"]"));

    public static final Target SELECT_EDUCATION_LEVEL = Target
            .the("Select Education Level")
            .located(By.id("education_code"));

    public static final Target INSTITUTE = Target
            .the("Education Institute")
            .located(By.id("education_institute"));

    public static final Target MAJOR_SPECIALIZATION = Target
            .the("Education Mayor / Specialization")
            .located(By.id("education_major"));

    public static final Target EDUCATION_YEAR = Target
            .the("Education Year")
            .located(By.id("education_year"));

    public static final Target GPA_SCORE = Target
            .the("Education GPA / Score")
            .located(By.id("education_gpa"));

    public static final Target EDUCATION_START_DATE = Target
            .the("Education Start Date")
            .located(By.id("education_start_date"));

    public static final Target EDUCATION_END_DATE = Target
            .the("Education End Date")
            .located(By.id("education_end_date"));

    public static final Target EDUCATION_SAVE_BTN = Target
            .the("Education Save Button")
            .located(By.id("btnEducationSave"));

    public static final Target ADD_SKILL_BTN = Target
            .the("Add Skill Button")
            .located(By.id("addSkill"));

    public static final Target DELETE_SKILL_BTN = Target
            .the("Delete Skill Button")
            .located(By.id("delSkill"));

    public static final Target SELECT_ALL_SKILLS = Target
            .the("Select All Skills")
            .located(By.xpath("//*[@id=\"skillCheckAll\"]"));

    public static final Target SELECT_SKILL = Target
            .the("Select Skill")
            .located(By.id("skill_code"));

    public static final Target YEARS_OF_EXPERIENCE = Target
            .the("Years of Experience")
            .located(By.id("skill_years_of_exp"));

    public static final Target SKILL_COMMENTS = Target
            .the("Skill Comments")
            .located(By.id("skill_comments"));

    public static final Target SKILL_SAVE_BTN = Target
            .the("Skill Save Button")
            .located(By.id("btnSkillSave"));

    public static final Target ADD_LANGUAGE_BTN = Target
            .the("Add Language Button")
            .located(By.id("addLanguage"));

    public static final Target DELETE_LANGUAGE_BTN = Target
            .the("Delete Language Button")
            .located(By.id("delLanguage"));

    public static final Target SELECT_ALL_LANGUAGES = Target
            .the("Select All Languages")
            .located(By.xpath("//*[@id=\"languageCheckAll\"]"));

    public static final Target SELECT_LANGUAGE = Target
            .the("Select Language")
            .located(By.id("language_code"));

    public static final Target SELECT_FLUENCY = Target
            .the("Select Language Fluency")
            .located(By.id("language_lang_type"));

    public static final Target SELECT_COMPETENCY = Target
            .the("Select Language Competency")
            .located(By.id("language_competency"));

    public static final Target LANGUAGE_COMMENTS = Target
            .the("Language Comments")
            .located(By.id("language_comments"));

    public static final Target LANGUAGE_SAVE_BTN = Target
            .the("Language Save Button")
            .located(By.id("btnLanguageSave"));

    public static final Target WORK_EXPERIENCE_VALIDATION = Target
            .the("Work Experience Validation")
            .located(By.xpath("//*[@id=\"frmDelWorkExperience\"]/table/tbody/tr/td[2]/a"));

    public static final Target EDUCATION_YEAR_VALIDATION = Target
            .the("Education Year Validation")
            .located(By.xpath("//*[@id=\"tblEducation\"]/table/tbody/tr[1]/td[3]"));

    public static final Target SKILL_YEARS_OF_EXPERIENCE_VALIDATION = Target
            .the("Skill Years Of Experience Validation")
            .located(By.xpath("//*[@id=\"frmDelSkill\"]/table/tbody/tr/td[3]"));

    public static final Target LANGUAGE_COMMENT_VALIDATION = Target
            .the("Language Comment Validation")
            .located(By.xpath("//*[@id=\"lang_data_table\"]/tbody/tr/td[5]"));

}
