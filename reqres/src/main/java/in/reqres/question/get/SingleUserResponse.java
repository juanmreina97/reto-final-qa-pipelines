package in.reqres.question.get;

import in.reqres.models.user.User;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class SingleUserResponse implements Question<User> {

    public static SingleUserResponse singleUserResponse() {
        return new SingleUserResponse();
    }

    @Override
    public User answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(User.class);
    }

}
